import React from 'react';
import { StyleSheet, Text, View,Image,ScrollView } from 'react-native';

const empresa = require('../../assets/imagens_app5/detalhe_empresa.png');

export default class ViewEmpresa extends React.Component {
    render() {
        return (
            <ScrollView style={styles.container}>
                <View style={styles.imgContato}>
                    <Image source={empresa}/>
                    <Text style={styles.txtTitulo}>Empresa</Text>
                </View>
                <View style={styles.detalheCliente}>
                    <Text style={styles.txtDetalheCliente}>Long Time.....</Text>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#FFF'
    },
    imgContato:{
        flexDirection:'row',
        marginTop:20,
        marginLeft:10,
        alignItems:'center'
    },
    txtTitulo:{
        color:'#EC7148',
        fontSize: 23,
        marginLeft: 20
    },
    detalheCliente:{
        padding:20,
        marginTop:10
    },
    txtDetalheCliente:{
        marginLeft:30,
        fontSize:18
    }

});