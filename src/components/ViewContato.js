import React from 'react';
import { StyleSheet, Text, View,Image,ScrollView } from 'react-native';

const contato = require('../../assets/imagens_app5/detalhe_contato.png');

export default class ViewContato extends React.Component {
    render() {
        return (
            <ScrollView style={styles.container}>
                <View style={styles.imgContato}>
                    <Image source={contato}/>
                    <Text style={styles.txtTitulo}>Contatos</Text>
                </View>
                <View style={styles.detalheCliente}>
                    <Text style={styles.txtDetalheCliente}>Tel: (11) 1234-5677</Text>
                    <Text style={styles.txtDetalheCliente}>Cel:(32) 12345-6789</Text>
                    <Text style={styles.txtDetalheCliente}>Email:llobatomaciel@gmai.com</Text>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#FFF'
    },
    imgContato:{
        flexDirection:'row',
        marginTop:20,
        marginLeft:10,
        alignItems:'center'
    },
    txtTitulo:{
        color:'#61BD8C',
        fontSize: 23,
        marginLeft: 20
    },
    detalheCliente:{
        padding:20,
        marginTop:10
    },
    txtDetalheCliente:{
        marginLeft:30,
        fontSize:18
    }

});