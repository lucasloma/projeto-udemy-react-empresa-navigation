import React from 'react';
import { StyleSheet, Text, View,Image,ScrollView } from 'react-native';

const detalheCliente = require('../../assets/imagens_app5/detalhe_cliente.png');
const cliente1 = require('../../assets/imagens_app5/cliente1.png');
const cliente2 = require('../../assets/imagens_app5/cliente2.png');

export default class ViewClientes extends React.Component {
    render() {
        return (
            <ScrollView style={styles.container}>
                <View>
                    <View style={styles.cabecalhoCliente}>
                        <Image source={detalheCliente}/>
                        <Text style={styles.txtTitulo}>Nossos Clientes</Text>
                    </View>
                    <View style={styles.detalheCliente}>
                        <Image source={cliente1}/>
                        <Text style={styles.txtDetalheCliente}>cliente 1</Text>
                    </View>
                    <View style={styles.detalheCliente}>
                        <Image source={cliente2}/>
                        <Text style={styles.txtDetalheCliente}>cliente 2</Text>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
container:{
    flex:1,
    backgroundColor:'#FFF'
},
    cabecalhoCliente:{
      flexDirection:'row',
        marginTop:20,
        alignItems:'center'
    },
    txtTitulo:{
        color:'#B9C941',
        fontSize: 23,
        marginLeft: 20
    },
    detalheCliente:{
        padding:20,
        marginTop:10
    },
    txtDetalheCliente:{
        marginLeft:30,
        fontSize:18
    }

});