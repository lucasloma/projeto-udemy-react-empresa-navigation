import React from 'react';
import { StyleSheet, View,Image,TouchableOpacity,ScrollView } from 'react-native';

const logo = require('../../assets/imagens_app5/logo.png');
const menuCliente = require('../../assets/imagens_app5/menu_cliente.png');
const menuContato = require('../../assets/imagens_app5/menu_contato.png');
const menuEmpresa = require('../../assets/imagens_app5/menu_empresa.png');
const menuServico = require('../../assets/imagens_app5/menu_servico.png');

export default class Principal extends React.Component {
    render() {
        const {navigation} = this.props;
        return (
            <View style={styles.container}>
                <View style={styles.logo}>
                    <Image source={logo}/>
                </View>
                <ScrollView>
                    <View style={styles.menu}>
                        <View style={styles.menuGrupo}>
                            <TouchableOpacity onPress={() => (navigation.navigate('viewClientes'))}>
                                <Image style={styles.imgMenu} source={menuCliente}/>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => (navigation.navigate('viewContato'))}>
                                <Image style={styles.imgMenu} source={menuContato}/>
                            </TouchableOpacity>
                        </View>
                        <View>
                            <View style={styles.menuGrupo}>
                                <TouchableOpacity onPress={() => (navigation.navigate('viewEmpresa'))}>
                                    <Image style={styles.imgMenu} source={menuEmpresa}/>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => (navigation.navigate('viewServico'))}>
                                    <Image style={styles.imgMenu} source={menuServico}/>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor:'#FFF'

    },
    logo:{
        alignItems:'center',
        padding:15
    },
    menu:{
        alignItems:'center'
    },
    menuGrupo:{
        flexDirection:'row',
        padding:10
    },
    imgMenu:{
        margin:15
    }
});