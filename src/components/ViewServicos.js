import React from 'react';
import { StyleSheet, Text, View,Image,ScrollView } from 'react-native';

const servico = require('../../assets/imagens_app5/detalhe_servico.png');

export default class ViewServicos extends React.Component {
    render() {
        return (
            <ScrollView style={styles.container}>
                <View style={styles.imgContato}>
                    <Image source={servico}/>
                    <Text style={styles.txtTitulo}>Serviços</Text>
                </View>
                <View style={styles.detalheCliente}>
                    <Text style={styles.txtDetalheCliente}>Lista Serviços</Text>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#FFF'
    },
    imgContato:{
        flexDirection:'row',
        marginTop:20,
        marginLeft:10,
        alignItems:'center'
    },
    txtTitulo:{
        color:'#19D1C8',
        fontSize: 23,
        marginLeft: 20
    },
    detalheCliente:{
        padding:20,
        marginTop:10
    },
    txtDetalheCliente:{
        marginLeft:30,
        fontSize:18
    }

});