import React from 'react';
import { createStackNavigator } from 'react-navigation'
import Principal from './src/components/Principal';
import ViewClientes from './src/components/ViewClientes';
import ViewContato from './src/components/ViewContato';
import ViewServicos from "./src/components/ViewServicos";
import ViewEmpresa from "./src/components/ViewEmpresa";

export default createStackNavigator({
    'Main':{
      screen:Principal,
    },
    'viewClientes':{
      screen:ViewClientes,
        navigationOptions:{
            headerStyle:{
                backgroundColor:'#B9C941',
                height:80,
            },
      }
    },
    'viewContato':{
        screen:ViewContato,
        navigationOptions:{
            headerStyle:{
                backgroundColor:'#61BD8C',
                height:80,
            },
        }
    },
    'viewServico':{
        screen:ViewServicos,
        navigationOptions:{
            headerStyle:{
                backgroundColor:'#19D1C8',
                height:80,
            },
        }
    },
    'viewEmpresa':{
        screen:ViewEmpresa,
        navigationOptions:{
            headerStyle:{
                backgroundColor:'#EC7148',
                height:80,
            },
        }
    }
    ,
},{
    navigationOptions: () => ({
        title: `L&A Consultoria`,
        headerStyle:{
            backgroundColor:'#CCC',
            height:80,
        },
        headerText:{
            fontSize: 18,
            color: '#000',
        }
    }),
});

